﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Util;
using System.Collections.Generic;
using Java.Util;
using System.Threading.Tasks;
using System;
using Android.Views;
using Android.Graphics;
using Android.Media;

namespace ktimer
{
    [Activity(Label = "ktimer", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        [Flags]
        enum Buttons
        {
            None = 0,
            Minute = 0x1,
            Second = 0x2,
            StartStop = 0x4,
        };
        enum Sound
        {
            Pip,
            //Po,
            Poon,
            Alarm,
        };
        TextView timeText;
        TextView statusText;
        Button minButton;
        Button secButton;
        Button goButton;
        ProgressBar progressBar;
        Handler handler = new Handler();
        Controller controller;
        Buttons buttonState = Buttons.None;
        SoundPool soundPool = new SoundPool(4, Stream.Notification, 0);
        Dictionary<Sound, int> soundId = new Dictionary<Sound, int>();
        Dictionary<Sound, int> streamId = new Dictionary<Sound, int>();
        AudioManager audioManager;
        Vibrator vibrator;
        long[] vibrationPattern = { 500, 500, 500, 500, 500, 500, 500, 500, 500, 500 };

        public override Java.Lang.Object OnRetainNonConfigurationInstance()
        {
            this.controller.StateChanged -= this.Controller_StateChanged;
            return this.controller;
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            this.timeText = FindViewById<TextView>(Resource.Id.textView1);
            this.minButton = FindViewById<Button>(Resource.Id.button1);
            this.secButton = FindViewById<Button>(Resource.Id.button2);
            this.goButton = FindViewById<Button>(Resource.Id.button3);
            this.statusText = FindViewById<TextView>(Resource.Id.textView2);
            this.progressBar = FindViewById<ProgressBar>(Resource.Id.progressBar1);

            var metrics = new DisplayMetrics();
            this.WindowManager.DefaultDisplay.GetMetrics(metrics);
            var shorter = (metrics.WidthPixels < metrics.HeightPixels) ? metrics.WidthPixels : metrics.HeightPixels;

            this.timeText.SetTextSize(ComplexUnitType.Px, shorter * 0.3f);
            this.timeText.Gravity = GravityFlags.CenterHorizontal | GravityFlags.CenterVertical;
#if DEBUG
            this.statusText.Visibility = ViewStates.Invisible;
#else
            this.statusText.Visibility = ViewStates.Invisible;
#endif

            foreach (var item in new[] { this.minButton, this.secButton, this.goButton })
            {
                item.SetTextSize(ComplexUnitType.Px, shorter * 0.06f);
                //item.SetTextSize(ComplexUnitType.Sp, 50.0f);
                item.Touch += this.ButtonTouched;
            }
            this.minButton.LongClick += (o, ea) => this.MinSecButtonTouched(true);
            this.secButton.LongClick += (o, ea) => this.MinSecButtonTouched(true);

            this.buttonState = Buttons.None;

            this.soundId[Sound.Pip] = this.soundPool.Load(this, Resource.Raw.pip, 1);
            //this.soundId[Sound.Po] = this.soundPool.Load(this, Resource.Raw.po, 1);
            this.soundId[Sound.Poon] = this.soundPool.Load(this, Resource.Raw.poon, 1);
            this.soundId[Sound.Alarm] = this.soundPool.Load(this, Resource.Raw.hato5, 1);

            this.audioManager = this.GetSystemService(AudioService) as AudioManager;
            this.vibrator = this.GetSystemService(VibratorService) as Vibrator;

            this.controller = this.LastNonConfigurationInstance as Controller;
            if (this.controller == null)
            {
                this.controller = new Controller();
                this.controller.StateChanged += this.Controller_StateChanged;
                this.controller.Reset();
            }
            else
            {
                this.controller.StateChanged += this.Controller_StateChanged;
                this.controller.Update();
            }
        }

        private void Controller_StateChanged(Controller.State prev, Controller.State current)
        {
            this.handler.Post(() =>
            {
                bool prevCounting = prev == Controller.State.UpCounting || prev == Controller.State.DownCounting;
                bool counting = current == Controller.State.UpCounting || current == Controller.State.DownCounting;
                bool alarming = current == Controller.State.Alarming;
                this.goButton.Text = Resources.GetString(
                    (counting || alarming) ? Resource.String.stop : Resource.String.start);
                int min = this.controller.CurrentSeconds / 60;
                int sec = this.controller.CurrentSeconds % 60;
                this.timeText.Text = string.Format("{0:00}:{1:00}", (min % 100), sec);
                this.statusText.Text = this.controller.StateText;

                if (prev != Controller.State.Alarming && current == Controller.State.Alarming)
                {
                    this.StartAlarm();
                }
                else if (prev == Controller.State.Alarming && current != Controller.State.Alarming)
                {
                    this.StopAlarm();
                }
                else if (counting)
                {
                    if(prevCounting)
                    {
                        this.soundPool.Play(this.soundId[Sound.Pip], 1.0f, 1.0f, 1, 0, 1.0f);
                    }

                    if (prev != Controller.State.TimeSetting && min > 0 && sec == 0)
                    {
                        this.soundPool.Play(this.soundId[Sound.Poon], 1.0f, 1.0f, 1, 0, 1.0f);
                    }
                    else if (current == Controller.State.DownCounting)
                    {
                        // 残り時間を表すバー
                        this.progressBar.Max = this.controller.InitialSeconds;
                        this.progressBar.Progress = this.controller.CurrentSeconds;
                    }
                    else if(current == Controller.State.UpCounting)
                    {
                        // 1分ごとに満タンになるバー
                        this.progressBar.Max = 60;
                        this.progressBar.Progress = sec;
                    }
                }
                else if(current == Controller.State.Zero)
                {
                    this.progressBar.Progress = 0;
                    this.StopAlarm();
                }
            });
        }

        private void ButtonTouched(object sender, View.TouchEventArgs ea)
        {
            if (sender == this.goButton)
            {
                if (ea.Event.Action == MotionEventActions.Down)
                {
                    this.soundPool.Play(this.soundId[Sound.Pip], 1.0f, 1.0f, 1, 0, 1.0f);
                    this.controller.StartStop();
                }
            }
            if (sender == this.minButton || sender == this.secButton)
            {
                if (ea.Event.Action == MotionEventActions.Down)
                {
                    this.buttonState |= (sender == this.minButton) ? Buttons.Minute : Buttons.Second;
                    if (this.buttonState.HasFlag(Buttons.Minute) && this.buttonState.HasFlag(Buttons.Second))
                    {
                        // 分・秒を同時押しされたら初期化
                        this.soundPool.Play(this.soundId[Sound.Pip], 1.0f, 1.0f, 1, 0, 1.0f);
                        this.controller.Reset();
                    }
                    else
                    {
                        this.MinSecButtonTouched(false);
                    }
                }
                else if (ea.Event.Action == MotionEventActions.Up)
                {
                    this.buttonState &= ~((sender == this.minButton) ? Buttons.Minute : Buttons.Second);
                }
            }

            ea.Handled = false;
        }

        void StartAlarm()
        {
            var volume = this.audioManager.GetStreamVolume(Stream.Notification);
            if (volume > 0)
            {
                this.streamId[Sound.Alarm] = this.soundPool.Play(this.soundId[Sound.Alarm], 1.0f, 1.0f, 1, 0, 1.0f);
            }
            else
            {
                this.vibrator.Vibrate(this.vibrationPattern, -1);
            }
            this.handler.Post(this.AlarmProc);
        }

        void StopAlarm()
        {
            if (this.streamId.ContainsKey(Sound.Alarm))
            {
                this.soundPool.Stop(this.streamId[Sound.Alarm]);
            }
            this.vibrator.Cancel();
            this.timeText.Visibility = ViewStates.Visible;
        }

        void AlarmProc()
        {
            if (this.controller.CurrentState == Controller.State.Alarming)
            {
                this.timeText.Visibility = this.timeText.Visibility == ViewStates.Visible ? ViewStates.Invisible : ViewStates.Visible;
                this.handler.PostDelayed(this.AlarmProc, 100);
            }
        }

        void MinSecButtonTouched(bool repeat)
        {
            this.soundPool.Play(this.soundId[Sound.Pip], 1.0f, 1.0f, 1, 0, 1.0f);

            bool next = true;
            if (this.buttonState == Buttons.Minute)
            {
                this.controller.Minute();
            }
            else if (this.buttonState == Buttons.Second)
            {
                this.controller.Second();
            }
            else
            {
                next = false;
            }

            if(next && repeat)
            {
                this.handler.PostDelayed(() => this.MinSecButtonTouched(true), 100);
            }
        }
    }
}

