﻿using System;
using System.Collections.Generic;

namespace ktimer
{
    class Controller : Java.Lang.Object
    {
        public enum State
        {
            Zero,
            UpCounting,
            UpPaused,
            TimeSetting,
            DownCounting,
            DownPaused,
            Alarming,
            AlarmStopped,
        }

        public event Action<State, State> StateChanged = delegate { };
        public State CurrentState { get { return this.state; } }
        public int CurrentSeconds { get { return this.currentSeconds; } }
        public int InitialSeconds { get { return this.initialSeconds; } }
        //public bool Alarming { get { return this.state == State.Alarming; } }
        //public bool Counting { get { return this.state == State.UpCounting || this.state == State.DownCounting; } }
        public string StateText { get { return this.state.ToString(); } }

        enum Event
        {
            Minute,
            Second,
            StartStop,
            Reset,
            Tick,
            Zero,
        }

        Dictionary<(State, Event), (State, Action<Event>)> stateMatrix;

        State state = State.Zero;
        int initialSeconds = 0;
        int currentSeconds = 0;
        TimeCounter timeCounter = null;

        public Controller()
        {
            this.stateMatrix = new Dictionary<(State, Event), (State, Action<Event>)>()
            {
                { (State.Zero, Event.Minute), (State.TimeSetting, this.ActionIncreaseTime) },
                { (State.Zero, Event.Second), (State.TimeSetting, this.ActionIncreaseTime) },
                { (State.Zero, Event.StartStop), (State.UpCounting, this.ActionCountStart) },
                { (State.Zero, Event.Reset), (State.Zero, this.ActionReset) },

                { (State.UpCounting, Event.Minute), (State.UpPaused, this.ActionCountPause) },
                { (State.UpCounting, Event.Second), (State.UpPaused, this.ActionCountPause) },
                { (State.UpCounting, Event.StartStop), (State.UpPaused, this.ActionCountPause) },
                { (State.UpCounting, Event.Reset), (State.Zero, this.ActionReset) },
                { (State.UpCounting, Event.Tick), (State.UpCounting, (ev) => { this.currentSeconds++; }) },

                { (State.UpPaused, Event.Minute), (State.TimeSetting, (ev) => { this.initialSeconds = this.currentSeconds; this.ActionIncreaseTime(ev); }) },
                { (State.UpPaused, Event.Second), (State.TimeSetting, (ev) => { this.initialSeconds = this.currentSeconds; this.ActionIncreaseTime(ev); }) },
                { (State.UpPaused, Event.StartStop), (State.UpCounting, this.ActionCountResume) },
                { (State.UpPaused, Event.Reset), (State.Zero, this.ActionReset) },

                { (State.TimeSetting, Event.Minute), (State.TimeSetting, this.ActionIncreaseTime) },
                { (State.TimeSetting, Event.Second), (State.TimeSetting, this.ActionIncreaseTime) },
                { (State.TimeSetting, Event.StartStop), (State.DownCounting, this.ActionCountStart) },
                { (State.TimeSetting, Event.Reset), (State.Zero, this.ActionReset) },

                { (State.DownCounting, Event.Minute), (State.DownPaused, this.ActionCountPause) },
                { (State.DownCounting, Event.Second), (State.DownPaused, this.ActionCountPause) },
                { (State.DownCounting, Event.StartStop), (State.DownPaused, this.ActionCountPause) },
                { (State.DownCounting, Event.Reset), (State.Zero, this.ActionReset) },
                { (State.DownCounting, Event.Tick), (State.DownCounting, (ev) => { this.currentSeconds--; }) },
                { (State.DownCounting, Event.Zero), (State.Alarming, this.ActionCountStop) },

                { (State.DownPaused, Event.Minute), (State.TimeSetting, (ev) => { this.initialSeconds = this.currentSeconds; this.ActionIncreaseTime(ev); }) },
                { (State.DownPaused, Event.Second), (State.TimeSetting, (ev) => { this.initialSeconds = this.currentSeconds; this.ActionIncreaseTime(ev); }) },
                { (State.DownPaused, Event.StartStop), (State.DownCounting, this.ActionCountResume) },
                { (State.DownPaused, Event.Reset), (State.Zero, this.ActionReset) },

                { (State.Alarming, Event.Minute), (State.AlarmStopped, this.ActionAlarmStop) },
                { (State.Alarming, Event.Second), (State.AlarmStopped, this.ActionAlarmStop) },
                { (State.Alarming, Event.StartStop), (State.TimeSetting, (ev) => { this.ActionAlarmStop(ev); this.ActionRevertTime(ev); }) },
                { (State.Alarming, Event.Reset), (State.Zero, this.ActionReset) },

                { (State.AlarmStopped, Event.Minute), (State.TimeSetting, this.ActionIncreaseTime) },
                { (State.AlarmStopped, Event.Second), (State.TimeSetting, this.ActionIncreaseTime) },
                { (State.AlarmStopped, Event.StartStop), (State.TimeSetting, this.ActionRevertTime) },
                { (State.AlarmStopped, Event.Reset), (State.Zero, this.ActionReset) },
            };
        }

        public void Minute()
        {
            this.UpdateState(Event.Minute);
        }

        public void Second()
        {
            this.UpdateState(Event.Second);
        }

        public void StartStop()
        {
            this.UpdateState(Event.StartStop);
        }

        public void Reset()
        {
            this.UpdateState(Event.Reset);
        }

        public void Update()
        {
            this.StateChanged(this.state, this.state);
        }

        void Tick()
        {
            this.UpdateState(Event.Tick);
            if(this.timeCounter.CurrentSeconds == 0)
            {
                this.UpdateState(Event.Zero);
            }
        }

        void ActionIncreaseTime(Event ev)
        {
            this.initialSeconds +=
                (ev == Event.Minute) ? 60 :
                (ev == Event.Second) ? 10 :
                0;
            this.currentSeconds = this.initialSeconds;
        }

        void ActionCountStart(Event ev)
        {
            this.ActionCountStop(ev);
            this.timeCounter = TimeCounter.StartNew(this.initialSeconds);
            this.timeCounter.Tick += this.Tick;
        }

        void ActionCountPause(Event ev)
        {
            this.timeCounter?.Pause();
        }

        void ActionCountResume(Event ev)
        {
            this.timeCounter?.Resume();
        }

        void ActionCountStop(Event ev)
        {
            if (this.timeCounter != null)
            {
                this.timeCounter.Pause();
                this.timeCounter.Tick -= this.Tick;
                this.timeCounter = null;
            }
        }

        void ActionAlarmStart(Event ev)
        {
            //TODO: アラーム鳴らす
        }

        void ActionAlarmStop(Event ev)
        {
            //TODO: アラーム止める
        }

        void ActionRevertTime(Event ev)
        {
            this.currentSeconds = this.initialSeconds;
        }

        void ActionReset(Event ev)
        {
            this.ActionCountStop(ev);
            this.ActionAlarmStop(ev);
            this.initialSeconds = 0;
            this.currentSeconds = 0;
        }

        void UpdateState(Event ev)
        {
            if (this.stateMatrix.TryGetValue((this.state, ev), out var nextStateAndAction))
            {
                var prev = this.state;
                this.state = nextStateAndAction.Item1;
                nextStateAndAction.Item2?.Invoke(ev);
                this.StateChanged(prev, this.state);
            }
        }
    }
}