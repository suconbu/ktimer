﻿using System;
using System.Threading;

namespace ktimer
{
    class TimeCounter
    {
        /// <summary>
        /// 現在の経過/残り秒数を取得します。
        /// </summary>
        public double CurrentSeconds { get { return this.currentSeconds; } }
        
        /// <summary>
        /// 一秒単位の時間経過と一時停止を通知します。
        /// </summary>
        public event Action Tick = delegate { };

        /// <summary>
        /// 一時停止中ならtrue、そうでなければfalseを返します。
        /// </summary>
        public bool Paused { get { return this.pausedTime != null; } }

        Timer timer;

        /// <summary>
        /// 計時開始した時間
        /// </summary>
        DateTime startedTime;

        /// <summary>
        /// 一時停止した時間
        /// </summary>
        DateTime? pausedTime;

        /// <summary>
        /// 初期秒数
        /// </summary>
        int initialSeconds = 0;

        /// <summary>
        /// 現在の秒数
        /// </summary>
        double currentSeconds = 0;

        /// <summary>
        /// 通知周期
        /// </summary>
        readonly uint eventInterval = 1000;

        /// <summary>
        /// 計時を開始します。
        /// 初期秒数が0の場合はカウントアップ、1以上の場合はカウントダウンの動作となります。
        /// </summary>
        public static TimeCounter StartNew(int initialSeconds = 0)
        {
            var self = new TimeCounter();
            self.initialSeconds = initialSeconds;
            self.currentSeconds = initialSeconds;
            self.pausedTime = null;
            Console.WriteLine("StartNew");
            self.timer = new Timer(
                (state) =>
                {
                    self.currentSeconds += self.initialSeconds == 0 ? 1 : -1;
                    self.currentSeconds = Math.Max(0.0, self.currentSeconds);
                    self.currentSeconds = Math.Floor(self.currentSeconds);
                    self.Tick();
                },
                null,
                self.eventInterval,
                self.eventInterval
                );
            self.startedTime = DateTime.Now;
            return self;
        }

        /// <summary>
        /// 計時を一時停止します。
        /// </summary>
        public void Pause()
        {
            if (this.pausedTime == null)
            {
                this.timer.Change(Timeout.Infinite, this.eventInterval);
                this.pausedTime = DateTime.Now;
                this.currentSeconds += this.GetOffsetSeconds(this.pausedTime.Value);
                this.Tick();
            }
        }

        /// <summary>
        /// 計時を再開します。
        /// </summary>
        public void Resume()
        {
            if (this.pausedTime != null)
            {
                var offset = (int)(this.GetOffsetSeconds(this.pausedTime.Value) * 1000);
                this.timer.Change(this.eventInterval - offset, this.eventInterval);
                this.pausedTime = null;
            }
        }

        public void Toggle()
        {
            if (this.pausedTime == null)
            {
                this.Pause();
            }
            else
            {
                this.Resume();
            }
        }

        TimeCounter() { }

        /// <summary>
        /// 通知間隔で区切った時に半端となる部分の秒数を取得します。
        /// </summary>
        double GetOffsetSeconds(DateTime time)
        {
            return ((int)(this.pausedTime.Value - this.startedTime).TotalMilliseconds % this.eventInterval) / 1000.0;
        }
    }
}